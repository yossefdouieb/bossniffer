import urllib
import urllib.request
import socket
import json
import threading
import subprocess
import os
from scapy.all import *

NUM_OF_PACKETS = 10
UDP_IP = '127.0.0.1'
UDP_PORT = 1212

ipLocation = {}


def udp_socket(all_packets):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(json.dumps(all_packets).encode(), (UDP_IP, UDP_PORT))


def sniff_and_sort():
    ip = ''
    all_packets = []
    packet = {
        'ip': '',
        'country': '',
        'Ingoing': '',
        'port': '',
        'size': '',
        'program': '',
        }

    sniff_data = sniff(count=NUM_OF_PACKETS,
                       lfilter=scapy_sniff_lfilter)

    # sniff_data.summary()

    for i in range(NUM_OF_PACKETS):
        ip = str(sniff_data[i][IP].dst).replace("'", '')
        packet['ip'] = ip

        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        try:
            result = subprocess.check_output('netstat -nb', shell=True,
                    stderr=subprocess.STDOUT).decode()
        except subprocess.CalledProcessError as e:
            raise RuntimeError("command '{}' return with error (code {}): {}".format(e.cmd,
                               e.returncode, e.output))

        # result = subprocess.check_output(['netstat', '-nb'], shell=False, startupinfo=startupinfo).decode()

        netstatData = result

        # result = subprocess.run(['netstat', '-nb'], stdout=subprocess.PIPE)
        # netstatData = result.stdout.decode()

        netstatData = netstatData.replace('\t', '').replace('\r', ''
                ).replace('\n', '')
        netstatData = netstatData.split(']')

        for program in netstatData:
            if ip in program.split('[')[0]:
                try:
                    packet['program'] = str(program.split('[')[1])
                except:
                    packet['program'] = 'Unknown'
        if packet['program'] == '':
            packet['program'] = 'Unknown'

        #finding size
        packet['size'] = int(sniff_data[i][IP].len)

        #determine if ingoing or outgoing
        if str(sniff_data[i][IP].src).replace("'", ''
                ).startswith('10.0') \
            or str(sniff_data[i][IP].src).replace("'", ''
                ).startswith('192.168') \
            or str(sniff_data[i][IP].src).replace("'", ''
                ).startswith('127.0'):
            packet['Ingoing'] = False
        else:
            packet['Ingoing'] = True
        #end of determine if ingoing or outgoing

        # getting location of the outgoing ip address

        if packet['Ingoing'] == False:
            if not ip in ipLocation:
                url = 'http://api.ipstack.com/' + ip \
                    + '?access_key=ce083452c4972fd6aa39ec0e761b4ead'
                data = urllib.request.urlopen(url).read()
                output = json.loads(data)
                print(output)
                print (output['country_name'], type(output['country_name']))
                if output['country_name'] == None:
                    packet['country'] = "Unknown"
                    ipLocation[ip] = "Unknown"
                else:
                    packet['country'] = output['country_name']
                    ipLocation[ip] = output['country_name']
            else:
                packet['country'] = ipLocation[ip]
        #end of location

        #finding port
        if UDP in sniff_data[i]:
            packet['port'] = str(sniff_data[i][UDP].dport)
        elif TCP in sniff_data[i]:
            packet['port'] = str(sniff_data[i][TCP].dport)
        #end of finding port
        
        all_packets.append(packet.copy())

    for i in range(NUM_OF_PACKETS):
        print (all_packets[i], '\n')

    return all_packets


def scapy_sniff_lfilter(packet):
    if IP in packet:
        return True
    else:
        return False


def main():
    all_packets = sniff_and_sort()
    udp_socket(all_packets)
    main()


main()
