import socket
import sqlite3
import sys
import json
import threading
import re
import collections
from time import strftime, localtime

#reading settings file 
settings = open("settings.dat", "r")
settingsData = settings.read()
settings.close()

#reading html (template.html) file
htmlFile = open("template.html", "r")
htmlFileData = htmlFile.read()
htmlFile.close()

#dictionaries for each section
traffic_per_ip = {}
traffic_per_agent_in = {}
traffic_per_agent_out = {}
traffic_per_country = {}
traffic_per_app = {}
traffic_per_port = {}

#time
timeStamp = strftime("%Y-%m-%d %H:%M:%S", localtime())
htmlFileData = htmlFileData.replace("%%TIMESTAMP%%", timeStamp)

def statistics(packets, agentName):
    conn = sqlite3.connect('DB.db')
    c = conn.cursor()

    newHtmlFileData = htmlFileData
    
    #traffic per ip
    for packet in packets:
        values = "(\'" + packet["ip"] + "\',\'" + packet["country"] + "\'," + str(int(packet["Ingoing"])) + "," + str(packet["port"]) + "," + str(packet["size"]) + ",\'" + packet["program"] + "\')"
        sqlQuery = "INSERT INTO packets VALUES " + str(values)
        print(sqlQuery)
        c.execute(str(sqlQuery))
        conn.commit()

        if packet["ip"] != "192.168.1.6":
            try:
                traffic_per_ip[packet["ip"]] += packet["size"]
            except:
                traffic_per_ip[packet["ip"]] = 0
                traffic_per_ip[packet["ip"]] += packet["size"]
    conn.close()

    old_traffic_ip = htmlFileData.split("Traffic Per IP")
    old_traffic_ip = old_traffic_ip[1].split("labels: ")
    old_traffic_bytes = old_traffic_ip[1].split("data: ")[1].split("}")[0].split("]")[0] + "]"
    old_traffic_ip = old_traffic_ip[1].split("]")[0] + "]"

    ip_list = []
    bytes_list = []

    for ip, num_bytes in traffic_per_ip.items():
        ip_list.append(ip)
        bytes_list.append(num_bytes)

    newHtmlFileData = newHtmlFileData.replace(old_traffic_ip, str(ip_list))
    newHtmlFileData = newHtmlFileData.replace(old_traffic_bytes, str(bytes_list))
    #end of traffic per ip

    #traffic per agent

    #incoming
    
    for packet in packets:
        if packet["Ingoing"] == True:
            try:
                traffic_per_agent_in[agentName] += packet["size"]
            except:
                traffic_per_agent_in[agentName] = 0
                traffic_per_agent_in[agentName] += packet["size"]
            
            old_traffic_agent = htmlFileData.split("<h2>Traffic Per Agent</h2>")
            old_traffic_agent = old_traffic_agent[1].split("(\"agents-section-incoming\")")
            old_traffic_agent = old_traffic_agent[1].split("labels: ")
            old_traffic_agent_bytes = old_traffic_agent[1].split("data: ")[1].split("}")[0].split("]")[0] + "]"
            old_traffic_agent = old_traffic_agent[1].split("]")[0] + "]"
            
            agent_list = []
            agent_bytes_list = []

            for agent, num_bytes in traffic_per_agent_in.items():
                agent_list.append(agent)
                agent_bytes_list.append(num_bytes)
            
            newHtmlFileData = newHtmlFileData.replace(old_traffic_agent, str(agent_list))
            newHtmlFileData = newHtmlFileData.replace(old_traffic_agent_bytes, str(agent_bytes_list))
            
        else:
            #outgoing
            try:
                traffic_per_agent_out[agentName] += packet["size"]
            except:
                traffic_per_agent_out[agentName] = 0
                traffic_per_agent_out[agentName] += packet["size"]
            
            old_traffic_agent = htmlFileData.split("<h2>Traffic Per Agent</h2>")
            old_traffic_agent = old_traffic_agent[1].split("(\"agents-section-outgoing\")")
            old_traffic_agent = old_traffic_agent[1].split("labels: ")
            old_traffic_agent_bytes = old_traffic_agent[1].split("data: ")[1].split("}")[0].split("]")[0] + "]"
            old_traffic_agent = old_traffic_agent[1].split("]")[0] + "]"
                
            agent_list = []
            agent_bytes_list = []

            for agent, num_bytes in traffic_per_agent_out.items():
                agent_list.append(agent)
                agent_bytes_list.append(num_bytes)

            newHtmlFileData = newHtmlFileData.replace(old_traffic_agent, str(agent_list))
            newHtmlFileData = newHtmlFileData.replace(old_traffic_agent_bytes, str(agent_bytes_list))
        #end of traffic per agent

    #Traffic Per Country
            
    for packet in packets:
        if packet["country"] != '':
            try:
                traffic_per_country[packet["country"]] += packet["size"]
            except:
                traffic_per_country[packet["country"]] = 0
                traffic_per_country[packet["country"]] += packet["size"]
    
    old_traffic_country = htmlFileData.split("<h2>Traffic Per Country</h2>")
    old_traffic_country = old_traffic_country[1].split("labels: ")
    old_country_bytes = old_traffic_country[1].split("data: ")[1].split("}")[0].split("]")[0] + "]"
    old_traffic_country = old_traffic_country[1].split("]")[0] + "]"

    country_list = []
    bytes_list = []

    for country, num_bytes in traffic_per_country.items():
        country_list.append(country)
        bytes_list.append(num_bytes)
    
    newHtmlFileData = newHtmlFileData.replace(old_traffic_country, str(country_list))
    newHtmlFileData = newHtmlFileData.replace(old_country_bytes, str(bytes_list))
    #End Traffic Per Country


    #Traffic Per App      
    for packet in packets:
        if packet["program"] != '':
            try:
                traffic_per_app[packet["program"]] += packet["size"]
            except:
                traffic_per_app[packet["program"]] = 0
                traffic_per_app[packet["program"]] += packet["size"]
    
    old_traffic_app = htmlFileData.split("<h2>Traffic Per App</h2>")
    old_traffic_app = old_traffic_app[1].split("labels: ")
    old_app_bytes = old_traffic_app[1].split("data: ")[1].split("}")[0].split("]")[0] + "]"
    old_traffic_app = old_traffic_app[1].split("]")[0] + "]"

    app_list = []
    bytes_list = []

    for app, num_bytes in traffic_per_app.items():
        app_list.append(app)
        bytes_list.append(num_bytes)
    
    newHtmlFileData = newHtmlFileData.replace(old_traffic_app, str(app_list))
    newHtmlFileData = newHtmlFileData.replace(old_app_bytes, str(bytes_list))
    #End Traffic Per App

    #Traffic Per Port      
    for packet in packets:
        if packet["port"] != '':
            try:
                traffic_per_port[packet["port"]] += packet["size"]
            except:
                traffic_per_port[packet["port"]] = 0
                traffic_per_port[packet["port"]] += packet["size"]
    
    old_traffic_port = htmlFileData.split("<h2>Traffic Per App</h2>")
    old_traffic_port = old_traffic_port[1].split("labels: ")
    old_port_bytes = old_traffic_port[1].split("data: ")[1].split("}")[0].split("]")[0] + "]"
    old_traffic_port = old_traffic_port[1].split("]")[0] + "]"

    port_list = []
    bytes_list = []

    for port, num_bytes in traffic_per_port.items():
        port_list.append(port)
        bytes_list.append(num_bytes)
    
    newHtmlFileData = newHtmlFileData.replace(old_traffic_port, str(port_list))
    newHtmlFileData = newHtmlFileData.replace(old_port_bytes, str(bytes_list))
    #End Traffic Per Port

    #alerts

    old_alerts = htmlFileData.split("<h2>Alerts</h2>")
    old_alerts = old_alerts[1].split("<div class=\"row\">")
    old_alerts = old_alerts[1].split("</div>")
    old_alerts = old_alerts[0].replace("\n", "").replace(" ", "")
    print("OLD:" + old_alerts)
    
    banIpList = settingsData.split("<<<>>>")[1].split("#")
    
    for packet in packets:
        for bannedIp in banIpList:
            print(packet["ip"], bannedIp.split(":")[1])
            if packet["ip"] == bannedIp.split(":")[1]:
                print("Match!")
                newHtmlFileData = newHtmlFileData.replace(old_alerts, str("[(" + agentName + "," + bannedIp + ")]"))
                break
                                                                           
    
    #time stamp
    newTimeStamp = strftime("%Y-%m-%d %H:%M:%S", localtime())
    newHtmlFileData = newHtmlFileData.replace(timeStamp, newTimeStamp)


    #saving the updated report
    htmlFileW = open("template.html", "w")
    htmlFileW.write(newHtmlFileData)
    htmlFileW.close()
    
def listen(udp_ip, udp_port, agentName):
    print("Listening to " + udp_ip + ":::\n")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((udp_ip, udp_port))

    while True:
        data, addr = sock.recvfrom(10240)
        #print("received message: ", data)

        print(agentName + ":\n")

        print(json.dumps(json.loads(data), indent = 4))

        thread = threading.Thread(target = statistics, args = (json.loads(data), agentName))
        thread.start()
        sock.close()
        listen(udp_ip, udp_port, agentName)

def main():
    #print(agents)
    #listening (with threading) to each agent
    agents = settingsData.split("<<<>>>")[0].split("#")
    for agent in range(len(agents) - 1):
        if re.search('[a-zA-Z]', agents[agent]) == None: 
            thread = threading.Thread(target = listen, args = (agents[agent], 1212, agents[agent + 1]))
            thread.start()
            agent += 1
    
main()
