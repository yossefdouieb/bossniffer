A program that monitors the netowrk usage of endpoints (clients) using scapy.

the client sniffs communication and store them in the below JSON format:

```
packet = {
        'ip': '', - ip address of the packet (scapy library)
        'country': '', - country (by calling the ipstack API)
        'Ingoing': '', - Boolean, represent if the packet is ingoing or outgoing
        'port': '', - port
        'size': '', - size of the packets, in bytes
        'program': '', - the software name that opened the connection
        }
```
an array of packets is sent to the server every period of time and the server is updating the HTML report accordingly.        

Demo:
![alt text](https://gitlab.com/yossefdouieb/bossniffer/-/raw/master/Animation.gif)
