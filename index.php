<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- SEO -->
    <meta name="description" content="150 words">
    <meta name="author" content="uipasta">
    <meta name="url" content="http://www.yourdomainname.com">
    <meta name="copyright" content="company name">
    <meta name="robots" content="index,follow">
    <title>Boss Sniffer Statistics</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" type="image/x-icon" href="images/favicon/apple-touch-icon.png">
    <!-- All CSS Plugins -->
    <link rel="stylesheet" type="text/css" href="css/plugin.css">
    <!-- Main CSS Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Google Web Fonts  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <!-- HTML5 shiv and Respond.js support IE8 or Older for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>

    <!-- Preloader Start -->
    <div class="preloader">
        <p>Loading...</p>
    </div>
    <!-- Preloader End -->
    <!-- Menu Section Start -->
    <header id="home">
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="logo">
                            <img src="images/logo.png" width="263" height="40">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="navigation-menu">
                            <div class="navbar">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a class="smoth-scroll" href="#home">Home <div class="ripple-wrapper"></div></a></li>
                                        <li>
                                            <a class="smoth-scroll" href="#agents-section-id">Agents</a>
                                        </li>
                                        <li>
                                            <a class="smoth-scroll" href="#countries-section-id">Countries</a>
                                        </li>
                                        <li>
                                            <a class="smoth-scroll" href="#ips-section-id">Ips</a>
                                        </li>
                                        <li>
                                            <a class="smoth-scroll" href="#apps-section-id">Apps</a>
                                        </li>
                                        <li>
                                            <a class="smoth-scroll" href="#ports-section-id">Ports</a>
                                        </li>
                                        <li>
                                            <a class="smoth-scroll" href="#alerts-section-id">Alerts</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Menu Section End -->
    <!-- Home Section Start -->
    <section id="home" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-2 col-md-4 col-sm-6 margin-left-setting">
                </div>
            </div>
            <div class="col-md-5 col-sm-6">
            </div>
        </div>
        </div>
    </section>
    <!-- Home Section End -->
    <!-- Experience Start -->
    <section class="section-space-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Boss Sniffer Statistics</h2>
                        <div class="divider dark">
                            <i class="icon-graduation"></i>
                        </div>
                        <p>Last update: 2018-12-20 11:43:49</p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- Experience End -->


    <script type="text/javascript">
        BACKGROUD_COLORS_LIST = [
        "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd",  "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7", "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ffb74d", "#ff8a65", "#e57373", "#f06292", "#ba68c8", "#9575cd"
        ]
    </script>

    <section id="agents-section-id" class="section-space-padding about">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Traffic Per Agent</h2>
                        <div class="divider dark">
                            <i class="icon-graph"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div style="width: 600px; overflow: hidden; margin: 0 auto;">
                        <canvas id="agents-section-incoming"></canvas>
                    </div>
                    <script type="text/javascript">
                    new Chart(document.getElementById("agents-section-incoming"), {
                        responsive: false,
                        type: 'bar',
                        data: {
                            labels: ['Agent 1'],
                            datasets: [{
                                label: "Bytes",
                                backgroundColor: BACKGROUD_COLORS_LIST,
                                data: [10756]
                            }]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            title: {
                                display: true,
                                text: 'Incoming Traffic Per Agent'
                            }
                        }
                    });
                    </script>
                </div>
                <div class="col-md-6">
                    <div style="width: 600px; overflow: hidden; margin: 0 auto;">
                        <canvas id="agents-section-outgoing"></canvas>
                    </div>
                    <script type="text/javascript">
                    new Chart(document.getElementById("agents-section-outgoing"), {
                        responsive: false,
                        type: 'bar',
                        data: {
                            labels: ['Agent 1'],
                            datasets: [{
                                label: "Bytes",
                                backgroundColor: BACKGROUD_COLORS_LIST,
                                data: [8324]
                            }]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            title: {
                                display: true,
                                text: 'Outgoing Traffic Per Agent'
                            }
                        }
                    });
                    </script>
                </div>
            </div>
        </div>
        </div>
    </section>
    <section id="countries-section-id" class="section-space-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Traffic Per Country</h2>
                        <div class="divider dark">
                            <i class="icon-graph"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div style="width: 600px; overflow: hidden; margin: 0 auto;">
                        <canvas id="countries-section-incoming"></canvas>
                    </div>
					<?php
   class MyDB extends SQLite3
   {
      function __construct()
      {
         $this->open('DB.db');
      }
   }
   $db = new MyDB();
   if(!$db){
      echo $db->lastErrorMsg();
   } else {
      //echo "Opened database successfully\n";
   }

   $sql =<<<EOF
      SELECT * from PACKETS;
EOF;

   $ret = $db->query($sql);
   $countrysArr = array("Unknown");
   $numOfBytes = array(0);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
      //echo gettype($row['COUNTRY']). ", COUNTRY = ". $row['COUNTRY'] ."\t". "SIZE = ". $row['SIZE'];
      //echo "<br>";

      // add the BR tag to print the values in a new line    
	  if ($row['COUNTRY'] != "")
	  {
		  if (!in_array($row['COUNTRY'], $countrysArr)) {
			array_push($countrysArr, $row['COUNTRY']);
			array_push($numOfBytes, 0);
		  }
		  else {
			  $arrIndex = array_search($row['COUNTRY'], $countrysArr);
			  $numOfBytes[$arrIndex] += $row['SIZE'];
		  }
	  }   
      //echo "PORT = ". $row['PORT'] ."\n";
      //echo "Ingoing = ". $row['INGOING'] ."\n\n";
	  
   }
   //print_r($countrysArr);
   //print_r($numOfBytes);
   //echo "Operation done successfully\n";
   $db->close();
   $showStatsJS = "<script type=\"text/javascript\">
                    new Chart(document.getElementById(\"countries-section-incoming\"), {
                        responsive: false,
                        type: 'pie',
                        data: {
                            labels: [";
	foreach ($countrysArr as $curr) 
	{
		$showStatsJS .= "'". $curr. "',";
	}
	$showStatsJS .= "],
                            datasets: [{
                                label: \"Bytes\",
                                backgroundColor: BACKGROUD_COLORS_LIST,
                                data: [";
	foreach ($numOfBytes as $curr) 
	{
		$showStatsJS .= $curr. ",";
	}
	$showStatsJS .= "]
                            }]
                        },
                        options: {
                            legend: {
                                display: true
                            },
                            title: {
                                display: true,
                                text: 'Traffic Per Country'
                            }
                        }
                    });
                    </script>";
	echo $showStatsJS;
   
?>
                    
                </div>
                
            </div>
        </div>
        </div>
    </section>
    <section id="ips-section-id" class="section-space-padding about">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Traffic Per IP</h2>
                        <div class="divider dark">
                            <i class="icon-graph"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-7">
                    <div style="width: 600px; overflow: hidden; margin: 0 auto;">
                        <canvas id="ips-section-outgoing"></canvas>
                    </div>
                    <script type="text/javascript">
                    new Chart(document.getElementById("ips-section-outgoing"), {
                        responsive: false,
                        type: 'pie',
                        data: {
                            labels: ['10.0.0.8', '216.58.207.78', '52.2.186.223', '216.58.210.3', '172.217.18.110', '52.230.80.159', '52.114.128.8', '52.114.158.52', '239.255.255.250', '10.0.0.138', '10.0.0.255', '198.23.101.146', '52.230.83.250', '65.55.163.78', '40.77.226.249'],
                            datasets: [{
                                label: "Bytes",
                                backgroundColor: BACKGROUD_COLORS_LIST,
                                data: [14753, 770, 92, 41, 41, 133, 2574, 40, 1232, 403, 514, 92, 152, 765, 1686]
                            }]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            title: {
                                display: true,
                                text: 'Traffic Per Ip'
                            }
                        }
                    });
                    </script>
                </div>
            </div>
        </div>
        </div>
    </section>
    <section id="apps-section-id" class="section-space-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Traffic Per App</h2>
                        <div class="divider dark">
                            <i class="icon-graph"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div style="width: 600px; overflow: hidden; margin: 0 auto;">
                        <canvas id="apps-section-incoming"></canvas>
                    </div>
                    <script type="text/javascript">
                    new Chart(document.getElementById("apps-section-incoming"), {
                        responsive: false,
                        type: 'bar',
                        data: {
                            labels: ['chrome.exe', 'python.exe', 'svchost.exe', 'OneDrive.exe', 'TeamViewer_Service.exe'],
                            datasets: [{
                                label: "Bytes",
                                backgroundColor: BACKGROUD_COLORS_LIST,
                                data: [996, 8245, 11802, 133, 2112]
                            }]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            title: {
                                display: true,
                                text: 'Traffic Per App'
                            }
                        }
                    });
                    </script>
                </div>
                
            </div>
        </div>
        </div>
    </section>
    <section id="ports-section-id" class="section-space-padding about">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Traffic Per Port</h2>
                        <div class="divider dark">
                            <i class="icon-graph"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                
                <div class="col-md-7">
                    <div style="width: 600px; overflow: hidden; margin: 0 auto;">
                        <canvas id="ports-section-outgoing"></canvas>
                    </div>
                    <script type="text/javascript">
                    new Chart(document.getElementById("ports-section-outgoing"), {
                        responsive: false,
                        type: 'bar',
                        data: {
                            labels: [443, 40027, 80, 40022, 40035],
                            datasets: [{
                                label: "Bytes",
                                backgroundColor: BACKGROUD_COLORS_LIST,
                                data: [6241607, 8318, 1594496, 351, 210]
                            }]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            title: {
                                display: true,
                                text: 'Traffic Per Port'
                            }
                        }
                    });
                    </script>
                </div>
            </div>
        </div>
        </div>
    </section>

	
	    <section id="alerts-section-id" class="section-space-padding about">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Alerts</h2>
                        <div class="divider dark">
                            <i class="icon-graph"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                
                 [('Dani','157.240.1.19')]
            </div>
        </div>
        </div>
    </section>

    <!-- Footer Start -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 uipasta-credit">
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer End -->
    <!-- Back to Top Start -->
    <a href="#" class="scroll-to-top"><i class="icon-arrow-up-circle"></i></a>
    <!-- Back to Top End -->
    <!-- All Javascript Plugins  -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/plugin.js"></script>
    <!-- Main Javascript File  -->
    <script type="text/javascript" src="js/scripts.js"></script>
</body>

</html>
